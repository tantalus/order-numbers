//
var app = angular.module("app", []);

app.controller("IndexCtrl", function($scope) {

    $scope.number = '';
    $scope.numberInvalid = false;
    $scope.numberFocus = true;
    $scope.numberInvalidMessage = '';
    $scope.numbers = [];
    $scope.i = 0;
    $scope.j = 0;
    $scope.iteration = null;

    $scope.addNumber = function()
    {
        if ($scope.number.trim() != "") {

            if ($scope.numbers.indexOf(parseInt($scope.number)) == -1) {

                $scope.numberInvalid = false;
                $scope.numbers.push(parseInt($scope.number));
                $scope.printNumber();
                Materialize.toast('Number "' + $scope.number + '" added correctly!', 3000)

            } else {
                $scope.numberInvalid = true;
                $scope.numberInvalidMessage = 'Duplicate number, please try again';
            }

            $scope.number = '';

        } else {

            $scope.numberInvalid = true;
            $scope.numberInvalidMessage = 'Enter a number';

        }
    };

    $scope.clearNumbers = function()
    {
        $scope.numbers = [];
        $("#panel-numbers").html("");
        $("#number").focus();
    };

    $scope.initOrder = function()
    {
        console.log('initOrder...');
        $scope.orderNumbers();
        $scope.iteration = setInterval($scope.orderNumbers, 400);
        $("#btn-order").addClass('disabled');
    };

    $scope.printNumber = function()
    {
        console.log('printNumber...');
        var panelNumbers = $("#panel-numbers");
        panelNumbers.html("");

        for (i = 0; i < $scope.numbers.length; i++) {
            panelNumbers.append(
                "<span class='num'>" + $scope.numbers[i] + "</span>"
            );
        }

        $('#number').focus();
    };

    $scope.orderNumbers = function()
    {
        console.log('orderNumbers...');

        var Panel = $("#panel-numbers");
        $scope.animationOrder();
        Panel.addClass('animation');

        console.log($scope.i + ' < ' + $scope.numbers.length);

        if ($scope.i < $scope.numbers.length) {

            if ($scope.j < ($scope.numbers.length - $scope.i)) {

                if ($scope.numbers[$scope.j] > $scope.numbers[$scope.j + 1]) {
                    aux = $scope.numbers[$scope.j];
                    $scope.numbers[$scope.j] = $scope.numbers[$scope.j + 1];
                    $scope.numbers[$scope.j + 1] = aux;
                }
                $scope.j++;

            } else {
                $scope.j = 0;
                $scope.i++;
            }

        } else {
            console.log('A');
            clearInterval($scope.iteration); // stop SetInterval
            $("#btn-order").removeClass('disabled');
            Panel.removeClass('animation');
            $scope.i = 0;
            $scope.j = 0;
        }

        console.log($scope.numbers);

        $scope.printNumber();
    };

    $scope.animationOrder = function()
    {
        console.log('animationOrder...');
        $("#animationDelays").remove();

        var totalNum = $scope.numbers.length;
        var time = 0;
        var styles = "";
        for (totalNum; totalNum > 0; totalNum--) {
            time += (parseFloat((0.03).toFixed(2)));
            styles += ".animation span.num:nth-child(" + totalNum + ") { animation-delay: " + time.toFixed(2) + "s; } ";
        }
        $("<style id='animationDelays'>" + styles + "</style>").appendTo("head");
    }

}).directive('restrictTo', function() {
    return {
        restrict: 'A',
        link: function (scope, element, arg) {
            var re = new RegExp(arg.restrictTo);
            var exclude = /Backspace|Enter|Tab|Delete|Del|ArrowUp|Up|ArrowDown|Down|ArrowLeft|Left|ArrowRight|Right/;

            element[0].addEventListener('keydown', function(event) {
                if ( ! exclude.test(event.key) && !re.test(event.key)) {
                    event.preventDefault();
                }
            });
        }
    }
});